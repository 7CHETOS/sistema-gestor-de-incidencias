@extends('layouts.sidebar')


@section('content')
<div id="aspp">
<div class="card">
    <div class="card-header">
        Sala de chat
    </div>

    <div class="card-body">
        <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}" :incidentid={{ $incidentID }}></chat-form>
        <div class="timeline">
            <chat-messages :messages.sync="messages" :incident_id={{ $incidentID }}></chat-messages>
        </div>
    </div>

    <div class="card-footer text-right">

    </div>

</div>
</div>

@php
    $state = App\Http\Controllers\IncidentController::getIncidentStatus($incidentID)->state;
    if ($state == 'Cerrada' || $state == 'Anulado') {
        echo('<button type="text" name="flag" hidden></button>');
    }
@endphp

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">

    jQuery( document ).ready(function( $ ) {

        if ( $("button[name='flag']").length ) {
            $("button").prop('disabled', true);
            $("textarea").prop('disabled', true);
        }

    });
</script>
@endsection

@section('styles')
<style type="text/css">
    .chatroom {
    height: 500px;
    width: 600px;
    padding: 10px 20px 5px 10px;
    overflow-y: auto;
    }

    .message {
    width: 470px;
    margin: 3px;
    list-style: none;
    padding: 10px;
    }
    .timeline dl{position:relative;top:0;padding:1px 0;margin:0}
    .timeline dl:before{position:absolute;top:0;bottom:0;left:50%;z-index:100;width:2px;margin-left:-1px;content:'';background-color:#ccd1d9}
    .timeline dl dt{position:relative;top:30px;z-index:200;width:120px;padding:3px 5px;margin:0 auto 30px;font-weight:400;color:#fff;text-align:center;background-color:#aab2bd;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px}
    .timeline dl dd{position:relative;z-index:200}
    .timeline dl dd .circ{position:absolute;top:40px;left:50%;z-index:200;width:22px;height:22px;margin-left:-11px;background-color:#4fc1e9;border:4px solid #f5f7fa;border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%}
    .timeline dl dd .time{position:absolute;top:31px;left:50%;display:inline-block;width:100px;padding:10px 10px;color:#4fc1e9}
    .timeline dl dd .events{position:relative;width:47%;padding:10px 10px 0;margin-top:1px;background-color:#fff;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px}
    .timeline dl dd .events:before{position:absolute;top:12px;width:0;height:0;content:'';border-style:solid;border-width:6px}
    .timeline dl dd .events .events-body{overflow:hidden;zoom:1}
    .timeline dl dd .events .events-body .events-heading{margin:0 0 10px;font-size:14px}
    .timeline dl dd.pos-right .time{margin-left:-100px;text-align:right}
    .timeline dl dd.pos-right .events{float:right}.timeline dl dd.pos-right .events:before{left:-12px;border-color:transparent #fff transparent transparent}
    .timeline dl dd.pos-left .time{margin-left:0;text-align:left}
    .timeline dl dd.pos-left .events{float:left}
    .timeline dl dd.pos-left .events:before{right:-12px;border-color:transparent transparent transparent #fff}@media screen and (max-width:767px){.timeline dl:before{left:60px}
    .timeline dl dt{margin:0 0 30px}
    .timeline dl dd .circ{left:60px}
    .timeline dl dd .time{left:0}
    .timeline dl dd.pos-left
    .time{padding:10px 0; margin-left:0;text-align:left}
    .timeline dl dd.pos-left .events{float:right;width:84%}
    .timeline dl dd.pos-left .events:before{left:-12px;border-color:transparent #fff transparent transparent}
    .timeline dl dd.pos-right .time{padding:10px 0; margin-left:0;text-align:left}
    .timeline dl dd.pos-right .events{float:right;width:84%}}
</style>
@endsection
