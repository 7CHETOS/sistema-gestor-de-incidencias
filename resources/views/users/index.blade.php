@extends('layouts.sidebar')

@section('content')
    {{-- @section('title', 'Users') --}}
    <div class="card">
        <div class="card-header">
            <div class="row">
                    <div class="col-sm">
                        <h4>Lista de usuarios</h4>
                    </div>
                    <div class="col-sm">
                        <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary float-right">Crear usuario</a>
                    </div>
            </div>
        </div>

        <div class="card-body">
            @if ($users->isNotEmpty())
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Email</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <form class="" action="{{ route('users.destroy', $user) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ route('users.show', $user) }}" class="btn btn-link"><span class="oi oi-eye"></span></a>
                                        <a href="{{ route('users.edit', $user) }}" class="btn btn-link"><span class="oi oi-pencil"></span></a>
                                        <button type="submit" class="btn btn-link" onclick="return confirm('Esta seguro de eliminar a este usuario?')"><span class="oi oi-trash"></span></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <li>There are no registered users</li>
            @endif
        </div>

        <div class="card-footer">
        </div>

    </div>
@endsection

@section('styles')
    <!-- datatables.net -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css">

@endsection

@section('scripts')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery( document ).ready(function( $ ) {
        $( '#tree-incidencia' ).removeClass( "active" );
        $( '#tree-usuario' ).addClass( "active" );
        // $(".treeview").first().addClass( "active" );
        $( '#li-manage' ).addClass( "active" );
    });
</script>

@endsection
