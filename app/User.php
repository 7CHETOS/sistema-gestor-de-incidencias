<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    /**
     * MESSAGE
     * Establecemos la relacion a muchos
     *
     * @var array
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * INCIDENT
     *
     * @var array
     */
    public function incidents()
    {
        return $this->belongsToMany(Incident::class);
    }

    /**
     * ROLE
     *
     * @var array
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * CLOSURE
     *
     * @var array
     */
    public function closure()
    {
        return $this->hasMany(Closure::class);
    }

    // Si mi rol no es validado
    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    // Nos permite iterar entre roles
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    // Valida si el usuario tiene el rol por el cual pregunto.
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    // Valida si el usuario tiene el rol por el cual pregunto.
    public function hasIncident($incident)
    {
        if ($this->incidents()->where('incident_id', $incident)->first()) {
            return true;
        }
        return false;
    }

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
