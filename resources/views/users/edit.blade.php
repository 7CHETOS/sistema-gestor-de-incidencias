@extends('layouts.sidebar')
@section('title', "Create user")
@section('content')

    <div class="card">
            <div class="card-header">
                <h4>Editar usuario</h4>
            </div>

            <div class="card-body">
                <form action="{{ url("users/{$user->id}") }}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Nombre: </label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Alberto Ramos" value="{{ old('name', $user->name) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email: </label>
                        <div class="col-sm-10">
                            <input type="email" name="email" id="email" class="form-control" placeholder="example@example.com" value="{{ old('email', $user->email) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Contraseña: </label>
                        <div class="col-sm-10">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary" name="button1">Actualizar usuario</button>

                </form>
            </div>

            <div class="card-footer">

                    <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>

            </div>
    </div>

@endsection

@section('scripts')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery( document ).ready(function( $ ) {
        $( '#tree-incidencia' ).removeClass( "active" );
        $( '#tree-usuario' ).addClass( "active" );
        // $(".treeview").first().addClass( "active" );
        $( '#li-manage' ).addClass( "active" );
    });
</script>

@endsection