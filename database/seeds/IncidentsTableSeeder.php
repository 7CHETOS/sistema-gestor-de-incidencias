<?php

use Illuminate\Database\Seeder;

//
use App\Incident;
use Faker\Factory as Faker;
use App\User;
use App\Message;

class IncidentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,200) as $index) {
            $incident = new Incident();
            $incident->category = $faker->randomElement($array = array ('Hardware','Software','Estructural', 'Otros'));
            $incident->urgency = $faker->randomElement($array = array ('Baja','Media','Alta'));
            $incident->state = $faker->randomElement($array = array ('Nueva','En Progreso'));
            $incident->subject = $faker->sentence;
            // $incident->detail = $faker->text($maxNbChars = 191);
            $incident->save();

            $user_id = $faker->numberBetween($min = 2, $max = 25);
            $user = User::find($user_id);
            $user->incidents()->attach($incident);

            $message = new Message;
            $message->message = 'Este es el detalle de la incidencia #'.$index;
            $message->user_id = $user_id;
            $message->incident_id = $incident->id;
            $message->save();
        }
    }
}
