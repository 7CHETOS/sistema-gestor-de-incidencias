<?php

use Illuminate\Database\Seeder;

//
use App\User;
use App\Role;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleUser = Role::where('name','user')->first();
        $roleAdmin = Role::where('name','admin')->first();

        $user = new User();
        $user->name = 'Eddy Chirinos';
        $user->email = 'admin';
        $user->password = bcrypt('admin');
        $user->save();
        $user->roles()->attach($roleAdmin);

        $user = new User();
        $user->name = 'Alvaro Montes';
        $user->email = 'user';
        $user->password = bcrypt('user');
        $user->save();
        $user->roles()->attach($roleUser);


        $faker = Faker::create();
    	foreach (range(3,25) as $index) {
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->password = bcrypt('asdasd');
            $user->save();
            $user->roles()->attach($roleUser);
	    }
    }
}
