<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    //withTrashed() me permite visualizar data que fue 'soft deleting' para este modelo
    public function users()
    {
        return $this->belongsToMany(User::class)->withTrashed();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function closure()
    {
        return $this->hasOne(Closure::class);
    }

    public function hasClosing($incidentId)
    {
        if ($this->closure()->where('incident_id', $incidentId)->first()) {
            return true;
        }
        return false;
    }

}
