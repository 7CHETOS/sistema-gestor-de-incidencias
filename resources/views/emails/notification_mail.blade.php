<strong>Detalles</strong>
<p>Categoria: {{$category}}</p>
<p>Urgencia: {{$urgency}}</p>
<p>Mensaje: {{$subject}}</p>
@if(isset($closedBy))
    <p>Detalles del cierre: {{$detailClosing}}</p>
    <p>Cerrado por: {{$closedBy}}</p>
@endif
